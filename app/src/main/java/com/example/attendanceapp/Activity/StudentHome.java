package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.attendanceapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class StudentHome extends AppCompatActivity {

    DatabaseReference databaseReference;
    ImageView imgView;
    TextView txtName;
    CardView rlTakeAttendance, rlMyAttendance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_home);

        initUI();
        loadUserData();

        rlTakeAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), TakeAttendance.class));
            }
        });

        rlMyAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(),StudentsAttendanceView.class));
                Intent i = new Intent(StudentHome.this, StudentsAttendanceView.class);
                i.putExtra("userid",FirebaseAuth.getInstance().getCurrentUser().getUid());
                startActivity(i);
            }
        });

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), Settings_student.class));
            }
        });
    }

    private void initUI() {
        imgView = findViewById(R.id.imageVIEW_student);
        txtName = findViewById(R.id.txtname);
        rlTakeAttendance = findViewById(R.id.crdTakeAttendance);
        rlMyAttendance = findViewById(R.id.crdMyAttendance);
    }

    private void loadUserData() {
        databaseReference= FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String url=dataSnapshot.child("url").getValue().toString().trim();
                String name=dataSnapshot.child("name").getValue().toString().trim();

                txtName.setText("Hi, "+name);
                Picasso.get().load(url).into(imgView);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}