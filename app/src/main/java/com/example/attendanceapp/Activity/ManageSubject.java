package com.example.attendanceapp.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.attendanceapp.Model.Subject;
import com.example.attendanceapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ManageSubject extends AppCompatActivity {

    EditText edtSubjectName, edtSubjectCode,txtSession;
    Button btnAddSubject, btnCansel, btnViewSubjects;
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_subject);
        
        initUI();
        
        btnAddSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SaveSubject();
            }
        });

        btnCansel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AdminHome.class));
            }
        });

        btnViewSubjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ViewSubjects.class));
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AdminHome.class));
            }
        });
    }

    private void SaveSubject() {




        Subject subject = new Subject();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Subjects");

        String sub_name = edtSubjectName.getText().toString().trim();
        String sub_code = edtSubjectCode.getText().toString().trim();

        if (TextUtils.isEmpty(sub_name)) {
            Toast.makeText(getApplication(), "Please Enter Subject Name!", Toast.LENGTH_SHORT).show();
        } else if(TextUtils.isEmpty(sub_code)){
            Toast.makeText(getApplication(), "Please Enter Subject Code!", Toast.LENGTH_SHORT).show();
        }else{
            subject.setSubjectName(sub_name);
            subject.setSubjectCode(sub_code);
            subject.setSessionCount("");
            subject.setTime("");
            subject.setDate("");
            subject.setStatus("live");
            subject.setAddedBy(FirebaseAuth.getInstance().getCurrentUser().getUid());

            databaseReference.child(edtSubjectCode.getText().toString().trim()).setValue(subject);
            Toast.makeText(ManageSubject.this, "Subject Added...", Toast.LENGTH_SHORT).show();
            edtSubjectName.setText("");
            edtSubjectCode.setText((""));
//        txtSession.setText("");
        }
    }

    private void initUI() {
        edtSubjectCode = findViewById(R.id.edtSubjeccode);
        edtSubjectName = findViewById(R.id.edtSubjecttName);
        btnAddSubject = findViewById(R.id.btnAddSubject);
        btnCansel = findViewById(R.id.btnCancel);
        btnViewSubjects = findViewById(R.id.btnViewSubject);
//        txtSession = findViewById(R.id.edtSession);
        btnBack = findViewById(R.id.btnBack);
    }
}