package com.example.attendanceapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.attendanceapp.R;

public class AdminHome extends AppCompatActivity {

    CardView genarateQR;
    CardView viewAttendance;
    CardView manageSubject;
    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);

        initUI();

        manageSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ManageSubject.class));
            }
        });

        genarateQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),GenarateQR.class));
            }
        });

        viewAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ViewStudents.class));
            }
        });

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),Settings_admin.class));
            }
        });
    }

    private void initUI() {
        genarateQR  =findViewById(R.id.crdGenarateQr);
        viewAttendance = findViewById(R.id.crdMyAttendance);
        manageSubject = findViewById(R.id.crdManageSub);
        imgView = findViewById(R.id.imageVIEW_admin1);
    }
}