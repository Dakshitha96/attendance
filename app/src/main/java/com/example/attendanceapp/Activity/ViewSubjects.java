package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.attendanceapp.Adapters.StudentsViewHolder;
import com.example.attendanceapp.Adapters.SubjectViewHolder;
import com.example.attendanceapp.Model.Subject;
import com.example.attendanceapp.Model.User;
import com.example.attendanceapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

public class ViewSubjects extends AppCompatActivity {

    private FirebaseRecyclerOptions<Subject> options;
    private FirebaseRecyclerAdapter<Subject, SubjectViewHolder> adapter;

    DatabaseReference dbRef;
    RecyclerView recyclerView;
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_subjects);

        loadSubjects();

        btnOk = findViewById(R.id.btnOk);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),ManageSubject.class));
            }
        });
    }

    private void loadSubjects() {

        Query queryfilter = FirebaseDatabase.getInstance().getReference("Subjects")
                .orderByChild("status")
                .equalTo("live");
        dbRef = FirebaseDatabase.getInstance().getReference().child("Subjects");
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        options = new FirebaseRecyclerOptions.Builder<Subject>().setQuery(queryfilter, Subject.class).build();
        adapter = new FirebaseRecyclerAdapter<Subject, SubjectViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull SubjectViewHolder holder, final int position, @NonNull final Subject model) {

                holder.txtSubCode.setText(model.getSubjectCode());
                holder.txtSub.setText(" - "+model.getSubjectName());

                holder.txtDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new AlertDialog.Builder(ViewSubjects.this)
                                .setTitle("Delete Subject")
                                .setMessage("Are you sure you want to delete this Subject?")


                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        DatabaseReference dbref = FirebaseDatabase.getInstance().getReference().child("Subjects").child(model.getSubjectCode());
                                        dbref.child("status").setValue("deleted");

                                    }
                                })

                                .setNegativeButton(android.R.string.no, null)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                });

            }

            @NonNull
            @Override
            public SubjectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_subject, parent, false);
                return new SubjectViewHolder(view);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}