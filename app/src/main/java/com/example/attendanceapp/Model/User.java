package com.example.attendanceapp.Model;

public class User {
    String regNo;
    String role;
    String status;
    String name;
    String url;

    public User() {
    }

    public User(String regNo, String role, String status, String name, String url) {
        this.regNo = regNo;
        this.role = role;
        this.status = status;
        this.name = name;
        this.url = url;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
