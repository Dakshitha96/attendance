package com.example.attendanceapp.Model;

public class Attendane {
    String SubCode;
    String count;
    String lastUpdatedat;

    public Attendane() {
    }

    public Attendane(String subCode, String count, String lastUpdatedat) {
        SubCode = subCode;
        this.count = count;
        this.lastUpdatedat = lastUpdatedat;
    }

    public String getSubCode() {
        return SubCode;
    }

    public void setSubCode(String subCode) {
        SubCode = subCode;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLastUpdatedat() {
        return lastUpdatedat;
    }

    public void setLastUpdatedat(String lastUpdatedat) {
        this.lastUpdatedat = lastUpdatedat;
    }
}
