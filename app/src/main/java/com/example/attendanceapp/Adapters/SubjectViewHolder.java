package com.example.attendanceapp.Adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;

import org.w3c.dom.Text;

public class SubjectViewHolder extends RecyclerView.ViewHolder{

    public TextView txtSub;
    public TextView txtSubCode;
    public TextView txtDelete;

    public SubjectViewHolder(@NonNull View itemView) {
        super(itemView);
        txtSub = itemView.findViewById(R.id.txtSub);
        txtSubCode = itemView.findViewById(R.id.txtSubcode);
        txtDelete = itemView.findViewById(R.id.txtDelete);
    }
}
