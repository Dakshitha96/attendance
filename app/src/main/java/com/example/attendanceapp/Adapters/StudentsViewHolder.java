package com.example.attendanceapp.Adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;

public class StudentsViewHolder extends RecyclerView.ViewHolder {
    public TextView txtStudentName;
    public StudentsViewHolder(@NonNull View itemView) {
        super(itemView);
        txtStudentName = itemView.findViewById(R.id.txtStudentName);
    }
}
