package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.attendanceapp.Model.Attendane;
import com.example.attendanceapp.Model.Qr;
import com.example.attendanceapp.Model.Subject;
import com.example.attendanceapp.R;
import com.example.attendanceapp.utils.ConnectionInfo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class GenarateQR extends AppCompatActivity {

    Button btnGenarateQr, btnSearch , btnCancel;
    Spinner spnSubject;
    ImageView imgQR, btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genarate_qr);

        initUi();
        loadSubjects();

        btnGenarateQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateClass();
                GenarateQrCode();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AdminHome.class));
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), AdminHome.class));
            }
        });

    }

    private void CreateClass() {

        String ssid = ConnectionInfo.wifiIpAddress(this);
        String SubjectCode = spnSubject.getSelectedItem().toString();
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Lectures");
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChild(SubjectCode)) {


                    rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String lastUpdatedat = dataSnapshot.child(SubjectCode).child("lastUpdatedat").getValue().toString().trim();
                            if (!lastUpdatedat.equals(date)){
                                String count = dataSnapshot.child(SubjectCode).child("count").getValue().toString().trim();
                                int newcount = Integer.parseInt(count)+1;
                                rootRef.child(SubjectCode).child("count").setValue(String.valueOf(newcount));
                                rootRef.child(SubjectCode).child("lastUpdatedat").setValue(date);
                                rootRef.child(SubjectCode).child("ssid").setValue(ssid);
                            }else {
                                rootRef.child(SubjectCode).child("ssid").setValue(ssid);
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                } else {
                    Qr qr = new Qr();
                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Lectures");

                    qr.setSsid(ssid);
                    qr.setSubCode(SubjectCode);
                    qr.setStatus("Online");
                    qr.setCount("1");
                    qr.setLastUpdatedat(date);

                    databaseReference.child(SubjectCode).setValue(qr);
                    Toast.makeText(GenarateQR.this, "Now Class is Online...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void GenarateQrCode() {
        String SubjectCode = spnSubject.getSelectedItem().toString();
        try {

            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap(SubjectCode, BarcodeFormat.QR_CODE, 400, 400);
            imgQR.setImageBitmap(bitmap);

        } catch (Exception e) {

        }
    }

    private void loadSubjects() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference fDatabaseRoot = database.getReference();

        fDatabaseRoot.child("Subjects").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final List<String> propertyAddressList = new ArrayList<String>();

                for (DataSnapshot addressSnapshot : dataSnapshot.getChildren()) {
                    if (addressSnapshot.child("status").getValue(String.class).equals("live")) {
                        String propertyAddress = addressSnapshot.child("subjectCode").getValue(String.class);
                        if (propertyAddress != null) {
                            propertyAddressList.add(propertyAddress);
                        }
                    }
                }

                ArrayAdapter<String> addressAdapter = new ArrayAdapter<String>(GenarateQR.this, android.R.layout.simple_spinner_item, propertyAddressList);
                addressAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spnSubject.setAdapter(addressAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initUi() {
        btnGenarateQr = findViewById(R.id.btnGenarateQr);
        spnSubject = findViewById(R.id.spn_subject);
        imgQR = findViewById(R.id.img_qr);
        btnCancel = findViewById(R.id.btnCancel);
        btnBack = findViewById(R.id.btnBack);
    }
}