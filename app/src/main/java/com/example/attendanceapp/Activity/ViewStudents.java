package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.attendanceapp.Adapters.StudentAttendanceViewHolder;
import com.example.attendanceapp.Adapters.StudentsViewHolder;
import com.example.attendanceapp.Model.Attendane;
import com.example.attendanceapp.Model.User;
import com.example.attendanceapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ViewStudents extends AppCompatActivity {

    private FirebaseRecyclerOptions<User> options;
    private FirebaseRecyclerAdapter<User, StudentsViewHolder> adapter;

    DatabaseReference dbRef, dbRefLectureCount,dbRefAttendance;
    RecyclerView recyclerView;
    EditText edtSearch;
    Button btnSearch, btnOk;
    ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_students);

        initUI();

        Query queryall = FirebaseDatabase.getInstance().getReference("Users");
        loadStudents(queryall);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Query queryfilter = FirebaseDatabase.getInstance().getReference("Users")
                        .orderByChild("name")
                        .equalTo(edtSearch.getText().toString().trim());
                loadStudents(queryfilter);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AdminHome.class));
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AdminHome.class));
            }
        });

    }

    private void initUI() {
        edtSearch = findViewById(R.id.edtSearch);
        btnSearch = findViewById(R.id.btnSearch);
        btnOk = findViewById(R.id.btnOk);
        btnBack = findViewById(R.id.btnBack);
    }

    private void loadStudents(Query query) {

//        dbRef = FirebaseDatabase.getInstance().getReference().child("Users");
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        options = new FirebaseRecyclerOptions.Builder<User>().setQuery(query, User.class).build();
        adapter = new FirebaseRecyclerAdapter<User, StudentsViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull StudentsViewHolder holder, final int position, @NonNull final User model) {

               holder.txtStudentName.setText(model.getName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String id = getRef(position).getKey();
                        Intent i = new Intent(ViewStudents.this, StudentsAttendanceView_admin.class);
                        i.putExtra("userid",id);
                        startActivity(i);
                    }
                });


            }

            @NonNull
            @Override
            public StudentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_students,parent,false);
                return new StudentsViewHolder(view);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}