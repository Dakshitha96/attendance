package com.example.attendanceapp.Model;

public class Subject {
    String date;
    String time;
    String SubjectName;
    String SubjectCode;
    String AddedBy;
    String SessionCount;
    String status;

    public Subject() {
    }

    public Subject(String date, String time, String subjectName, String subjectCode, String addedBy, String sessionCount) {
        this.date = date;
        this.time = time;
        SubjectName = subjectName;
        SubjectCode = subjectCode;
        AddedBy = addedBy;
        SessionCount = sessionCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getSubjectCode() {
        return SubjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        SubjectCode = subjectCode;
    }

    public String getAddedBy() {
        return AddedBy;
    }

    public void setAddedBy(String addedBy) {
        AddedBy = addedBy;
    }

    public String getSessionCount() {
        return SessionCount;
    }

    public void setSessionCount(String sessionCount) {
        SessionCount = sessionCount;
    }
}
