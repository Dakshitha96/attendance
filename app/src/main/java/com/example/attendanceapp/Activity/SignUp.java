package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.attendanceapp.Model.User;
import com.example.attendanceapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import static android.content.ContentValues.TAG;

public class SignUp extends AppCompatActivity {

    EditText edtRegNo;
    EditText edtEmail;
    EditText edtPassword;
    EditText edtConfirmPassword;
    EditText edtName;
    Button btnSignUp;
    ProgressBar progressbar;
    TextView txtHaveAnAcc;

    ImageView ImgUserPhoto;
    static int PReqCode = 1;
    static int REQUESCODE = 1;
    Uri pickedImgUri;

    FirebaseAuth firebaseAuth;
    DatabaseReference dbRef;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initUi();

        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("", "onClick: " );
                if (Build.VERSION.SDK_INT >= 22) {
                    checkAndRequestForPermission();
                    Log.e(TAG, "onClick: per,ission");
                } else {
                    openGallery();
                    Log.e(TAG, "onClick: galery");
                }
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth = FirebaseAuth.getInstance();
                progressbar.setVisibility(View.VISIBLE);
                signUp();
            }
        });

        txtHaveAnAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });
    }

    private void signUp() {

        String regNo = edtRegNo.getText().toString();
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();
        String confirmPassword = edtConfirmPassword.getText().toString();

        if (TextUtils.isEmpty(regNo)){
            Toast.makeText(SignUp.this, "Please Enter register number", Toast.LENGTH_SHORT).show();
        }else  if (TextUtils.isEmpty(email)) {
            Toast.makeText(SignUp.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        } else if (TextUtils.isEmpty(password)) {
            Toast.makeText(SignUp.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
        } else if (password.length() < 6) {
            Toast.makeText(SignUp.this, "Password too short", Toast.LENGTH_SHORT).show();
        } else if (password.equals(confirmPassword)) {
            firebaseAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(SignUp.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
//                                Toast.makeText(SignUp.this, "Register Successfully, Please check your email for verification", Toast.LENGTH_SHORT).show();
                                UpdateUserDate();
                            } else {
                                Toast.makeText(SignUp.this, "Authentication Failed", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private void UpdateUserDate(){
        StorageReference mStorage = FirebaseStorage.getInstance().getReference().child("users_photos");
        final StorageReference imageFilePath = mStorage.child(pickedImgUri.getLastPathSegment());

        imageFilePath.putFile(pickedImgUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                //image upload succfully
                //get img url
                imageFilePath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(final Uri uri) {

                        //uri contain user image url
                        UserProfileChangeRequest profileUpdate = new UserProfileChangeRequest.Builder()
                                .setDisplayName(edtName.getText().toString())
                                .setPhotoUri(uri)
                                .build();
                        Log.e("test", "updateUserInfo: " + uri.toString());

                        final FirebaseUser currentuser = FirebaseAuth.getInstance().getCurrentUser();

                        currentuser.updateProfile(profileUpdate)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            SaveUserData(uri.toString());
                                        }
                                    }
                                });
                    }
                });
            }
        });
    }

    private void SaveUserData(String url) {
        user= new User();
        final String currentuser = FirebaseAuth.getInstance().getCurrentUser().getUid();

        dbRef= FirebaseDatabase.getInstance().getReference().child("Users");

        user.setRegNo(edtRegNo.getText().toString().trim());
        user.setRole("student");
        user.setStatus("pending");
        user.setName(edtName.getText().toString());
        user.setUrl(url);

        dbRef.child(currentuser).setValue(user);
        Toast.makeText(SignUp.this, "Register Successfully!!!", Toast.LENGTH_SHORT).show();
        progressbar.setVisibility(View.INVISIBLE);
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }

    private void openGallery() {
        Log.e(TAG, "openGallery: ");
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESCODE);

    }

    private void checkAndRequestForPermission() {

        if (ContextCompat.checkSelfPermission(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(SignUp.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(SignUp.this, "Please accept fr required permission", Toast.LENGTH_SHORT).show();

            } else {
                ActivityCompat.requestPermissions(SignUp.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PReqCode);
            }
        } else
            openGallery();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {
            //the user has succesfully picked an image
            //we need to save its reference to a uri varialblr
            pickedImgUri = data.getData();
            ImgUserPhoto.setImageURI(pickedImgUri);
        }
    }

    private void initUi() {
        edtRegNo = findViewById(R.id.edtRegNo);
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        btnSignUp = findViewById(R.id.btSignup);
        progressbar = findViewById(R.id.progress);
        txtHaveAnAcc = findViewById(R.id.txtHaveAnAcc);
        edtName = findViewById(R.id.edtName);
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
    }
}