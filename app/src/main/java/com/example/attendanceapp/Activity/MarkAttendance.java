package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.attendanceapp.Model.Attendane;
import com.example.attendanceapp.Model.Subject;
import com.example.attendanceapp.R;
import com.example.attendanceapp.utils.ConnectionInfo;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;


public class MarkAttendance extends AppCompatActivity {

    TextView txtSubject, txtmsg, txtSubjectCode;
    LinearLayout layout_attendaceMark;
    ImageView imgAuth;
    Button btnOk;

    DatabaseReference databaseReference;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mark_attendance);

        String qr = getIntent().getStringExtra("qr");

        initUI();
        LoadSubject(qr);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),StudentHome.class));
            }
        });

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(MarkAttendance.this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                txtmsg.setText("Authentication error : " + errString);
                Toast.makeText(MarkAttendance.this, "Authentication error: " + errString, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                txtmsg.setText("Authentication succeed ");
                imgAuth.setImageResource(R.drawable.tick);
                btnOk.setVisibility(View.VISIBLE);
                Toast.makeText(MarkAttendance.this, "Authentication succeed ", Toast.LENGTH_LONG).show();

                saveAttendance(qr);

            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                txtmsg.setText("Authentication failed ");
                Toast.makeText(MarkAttendance.this, "Authentication failed ", Toast.LENGTH_LONG).show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Confirm Using Your Fingerprint")
                .setSubtitle("You can use your fingerprint to confirm making attendance through this app.")
                .setNegativeButtonText("Use Password")
                .build();

        imgAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                biometricPrompt.authenticate(promptInfo);
            }
        });


    }

    private void saveAttendance(String qr) {
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference().child("Attendance").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        rootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.hasChild(qr)){
                    updateAttendaceCount(qr);
                }else {
                    Attendane attendane = new Attendane();
                    attendane.setSubCode(qr);
                    attendane.setCount("1");
                    attendane.setLastUpdatedat(date);

                    rootRef.child(qr).setValue(attendane);
                    Toast.makeText(MarkAttendance.this, "Attendance marked...", Toast.LENGTH_SHORT).show();
                    txtmsg.setText("Attendance Marked succeed");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void updateAttendaceCount(String qr) {
        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());


        DatabaseReference dbref = FirebaseDatabase.getInstance().getReference().child("Attendance").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(qr);
        dbref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String lastupdateedat = dataSnapshot.child("lastUpdatedat").getValue().toString().trim();
                if (!lastupdateedat.equals(date)) {
                    String count = dataSnapshot.child("count").getValue().toString().trim();
                    int newcount = Integer.parseInt(count)+1;

                    dbref.child("count").setValue(String.valueOf(newcount));
                    dbref.child("lastUpdatedat").setValue(date);
                    Toast.makeText(MarkAttendance.this, "Attendance marked...", Toast.LENGTH_SHORT).show();
                    txtmsg.setText("Attendance Marked succeed");
                }else {
                    Toast.makeText(MarkAttendance.this, "Already marked attendance", Toast.LENGTH_SHORT).show();
                    txtmsg.setText("Already marked attendance");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void LoadSubject(String qr) {
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Subjects").child(qr);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String subjectName = dataSnapshot.child("subjectName").getValue().toString().trim();
                String subjectCode = dataSnapshot.child("subjectCode").getValue().toString().trim();
                txtSubject.setText(subjectName);
                txtSubjectCode.setText(subjectCode);
                checkStudentInorOut(qr);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void checkStudentInorOut(String qr) {
        String myssid = ConnectionInfo.wifiIpAddress(this);
        Log.e("", "my ssid: " + myssid);

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Lectures").child(qr);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String ssid = dataSnapshot.child("ssid").getValue().toString().trim();
                Log.e("", "class ssid: " + ssid);
                if (myssid.equals(ssid)) {
                    txtmsg.setText("Please click on image for authenticate");
                } else {
                    new AlertDialog.Builder(MarkAttendance.this)
                            .setTitle("Warning!!!")
                            .setMessage("You are in out of class...")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(getApplicationContext(), StudentHome.class));
                                }
                            })

                            .setNegativeButton(android.R.string.no, null)
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initUI() {

        txtSubject = findViewById(R.id.txtSubject);
        layout_attendaceMark = findViewById(R.id.layout_attendaceMark);
        imgAuth = findViewById(R.id.imgAuth);
        txtmsg = findViewById(R.id.txtmsg);
        btnOk = findViewById(R.id.btnOk);
        txtSubjectCode = findViewById(R.id.txtSubCode);
    }
}