package com.example.attendanceapp.Model;

public class Qr {
    String subCode;
    String ssid;
    String status;
    String count;
    String lastUpdatedat;

    public Qr() {
    }

    public Qr(String subCode, String ssid, String status, String count, String lastUpdatedat) {
        this.subCode = subCode;
        this.ssid = ssid;
        this.status = status;
        this.count = count;
        this.lastUpdatedat = lastUpdatedat;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLastUpdatedat() {
        return lastUpdatedat;
    }

    public void setLastUpdatedat(String lastUpdatedat) {
        this.lastUpdatedat = lastUpdatedat;
    }
}
