package com.example.attendanceapp.Adapters;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.attendanceapp.R;


public class StudentAttendanceViewHolder extends RecyclerView.ViewHolder {

    public TextView txtSubject;
    public TextView txtPrecentage;
    public TextView txtlecCount;
    public TextView txtAttendanceCount;

    public StudentAttendanceViewHolder(@NonNull View itemView) {
        super(itemView);

        txtPrecentage = itemView.findViewById(R.id.txtPrecentage);
        txtSubject = itemView.findViewById(R.id.txtsubject);
    }
}
