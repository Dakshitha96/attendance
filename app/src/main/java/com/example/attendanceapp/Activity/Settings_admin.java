package com.example.attendanceapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.example.attendanceapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

public class Settings_admin extends AppCompatActivity {

    DatabaseReference databaseReference;
    ImageView imgView;
    TextView txtName;
//    CardView crdResetPassword;
    CardView crdLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_admin);

        initUI();

//        loadUserData();

//        crdResetPassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getApplicationContext(),ForgetPassword.class));
//            }
//        });

        crdLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
            }
        });

        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),AdminHome.class));
            }
        });
    }

    private void initUI() {
        imgView = findViewById(R.id.imageVIEW_admin);
        txtName = findViewById(R.id.txtname);
        crdLogout = findViewById(R.id.crdLogout);
//        crdResetPassword = findViewById(R.id.crd_resetPW);
    }

//    private void loadUserData() {
//        databaseReference= FirebaseDatabase.getInstance().getReference().child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
//        databaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//
//                String url=dataSnapshot.child("url").getValue().toString().trim();
//                String name=dataSnapshot.child("name").getValue().toString().trim();
//
//                txtName.setText("Welcome, "+name);
//                Picasso.get().load(url).into(imgView);
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });
//    }
}