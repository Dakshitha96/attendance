package com.example.attendanceapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.attendanceapp.Adapters.StudentAttendanceViewHolder;
import com.example.attendanceapp.Model.Attendane;
import com.example.attendanceapp.R;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;



public class StudentsAttendanceView extends AppCompatActivity {


    private FirebaseRecyclerOptions<Attendane> options;
    private FirebaseRecyclerAdapter<Attendane, StudentAttendanceViewHolder> adapter;

    DatabaseReference dbRef, dbRefLectureCount,dbRefAttendance, databaseReference;
    RecyclerView recyclerView;
    TextView txtName;
    Button btnOk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_attendance_view);

        txtName = findViewById(R.id.txtname);
        btnOk = findViewById(R.id.btnOk);

        String userid = getIntent().getStringExtra("userid");
        loadAttendace(userid);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),StudentHome.class));
            }
        });

    }






    private void loadAttendace(String userid) {

        dbRef = FirebaseDatabase.getInstance().getReference().child("Attendance").child(userid);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        options = new FirebaseRecyclerOptions.Builder<Attendane>().setQuery(dbRef, Attendane.class).build();
        adapter = new FirebaseRecyclerAdapter<Attendane, StudentAttendanceViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull StudentAttendanceViewHolder holder, final int position, @NonNull final Attendane model) {
//                holder.txtSubject.setText(model.getSubCode());

                ////get lecture count
                dbRefLectureCount= FirebaseDatabase.getInstance().getReference().child("Lectures").child(model.getSubCode());
                dbRefLectureCount.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String lecCount = dataSnapshot.child("count").getValue().toString().trim();

                        ////get atteandance count
                        dbRefAttendance= FirebaseDatabase.getInstance().getReference().child("Attendance").child(userid).child(model.getSubCode());
                        dbRefAttendance.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                String attendanceCount = dataSnapshot.child("count").getValue().toString().trim();
                                Float precentage = Float.parseFloat(attendanceCount)/Float.parseFloat(lecCount)*100;
                                String converted = String.format("%.2f", precentage);
                                holder.txtPrecentage.setText(converted+"%");

                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        ////
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                dbRefAttendance= FirebaseDatabase.getInstance().getReference().child("Subjects").child(model.getSubCode());
                dbRefAttendance.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String subjectName = dataSnapshot.child("subjectName").getValue().toString().trim();
                        holder.txtSubject.setText(subjectName);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            }

            @NonNull
            @Override
            public StudentAttendanceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_studentattendance,parent,false);
                return new StudentAttendanceViewHolder(view);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}